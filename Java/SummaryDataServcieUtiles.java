@Service
public class SummaryDataServcieUtiles {
    /***
     * 核心业务代码省略
     * */
    public static void ValidateSummaryData(Integer orderId,Integer quantity) {
        boolean isOrderValid = checkOrder(orderId, quantity);
        if (isOrderValid) {
            boolean isPaymentSuccessful = processPayment(orderId, quantity);
            if (isPaymentSuccessful) {
                boolean isInventoryUpdated = updateInventory(orderId, quantity);
                if (isInventoryUpdated) {
                  //此处省略业务逻辑代码35行
                } else {
                    //此处省略业务逻辑代码15行
                }
            } else {
                //此处省略业务逻辑代码32行
            }
        } else {
            //此处省略业务逻辑代码12行
        }
    }

    private static boolean checkOrder(Integer orderId, Integer quantity) {
        // 此处省略业务逻辑代码15行
        return orderId % quantity == 0;
    }

    private static boolean processPayment(Integer orderId, Integer quantity) {
        // 此处省略业务逻辑代码32行
        return orderId % quantity != 0;
    }

    private static boolean updateInventory(Integer orderId, Integer quantity) {
        // 此处省略业务逻辑代码25行
        return orderId % quantity == 0;
    }
}

