//Field字段是varchar存储了3个值的逗号分隔列表
//应该很明显为什么它太可怕了

SELECT *
FROM
    SomeTable
WHERE
    Field LIKE @Param + ',%'
    OR
    Field LIKE '%,' + @Param + ',%'
    OR
    Field LIKE '%,' + @Param;