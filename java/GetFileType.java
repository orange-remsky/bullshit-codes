    public static String GetFileType(String originalFilename) {
        //获取最后一个.的位置
        int lastIndexOf = originalFilename.lastIndexOf(".");
        if (lastIndexOf < 0) {
            return "";
        }
        //获取文件的后缀名 .jpg
        String suffix = originalFilename.substring(lastIndexOf);
        if (suffix.equals(".gz")) {
            return ".gz";
        } else if (suffix.equals(".zip")) {
            return ".zip";
        } else if (suffix.equals(".csv")) {
            return ".csv";
        } else if (suffix.equals(".xml")) {
            return ".xml";
        } else if (suffix.equals(".xls")) {
            return ".xls";
        } else if (suffix.equals(".xlsx")) {
            return ".xlsx";
        } else {
            return "";
        }
    }