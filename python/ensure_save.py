from time import sleep


def ensure_save(times=3):
    '''
    重要的事情保存三遍
    :param times: 执行次数
    :return:
    '''
    for i in range(times):
        save()
        print('Saved {0} times!'.format(i + 1))
        sleep(1)
