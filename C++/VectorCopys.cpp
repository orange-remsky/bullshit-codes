// 这个故事告诉我们，变量数据过大的时候请直接用指针...
// 这是某游戏的扩展的一部分代码
// 其实核心看点就那两条注释

/// @author Ceyase

// 这里省略一些include

vector<vector<float>> audioList = {};

dllg double mv_load(const char* songPath) {
    std::shared_ptr<AudioData> fileData = std::make_shared<AudioData>();
    SoundwaveIO loader;
    loader.Load(fileData.get(), U8ToUnicode(songPath));
    audioList.push_back(fileData->samples); // 这里加载了一个将近30MB的文件，直接把所有文件数据全扔进audioList这个容器里了
    return audioList.size() - 1;
}

dllg vector<double> mv_get_data(int AudioID) {
    vector<float> preSamples = audioList[AudioID]; // 这简简单单一句话，就要求游戏在一帧内重新在内存里拷贝30MB的数据
    // 此处省略一些处理代码
    return resultSamples;
}